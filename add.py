class DestinoCulinario:
    def __init__(self, id, nombre, tipo_cocina, ingredientes, precio_minimo, precio_maximo,
                 popularidad, disponibilidad, id_ubicacion, imagen):
        self.id = id
        self.nombre = nombre
        self.tipo_cocina = tipo_cocina
        self.ingredientes = ingredientes
        self.precio_minimo = precio_minimo
        self.precio_maximo = precio_maximo
        self.popularidad = popularidad
        self.disponibilidad = disponibilidad
        self.id_ubicacion = id_ubicacion
        self.imagen = imagen

class Actividad:
    def __init__(self, id, nombre, destino_id, hora_inicio):
        self.id = id
        self.nombre = nombre
        self.destino_id = destino_id
        self.hora_inicio = hora_inicio

class RutaVisita:
    def __init__(self, id, nombre, destinos):
        self.id = id
        self.nombre = nombre
        self.destinos = destinos

class Ubicacion:
    def __init__(self, id, direccion, coordenadas):
        self.id = id
        self.direccion = direccion
        self.coordenadas = coordenadas

class Usuario:
    def __init__(self, id, nombre, apellido, historial_rutas):
        self.id = id
        self.nombre = nombre
        self.apellido = apellido
        self.historial_rutas = historial_rutas

class Review:
    def __init__(self, id, id_destino, id_usuario, calificacion, comentario, animo):
        self.id = id
        self.id_destino = id_destino
        self.id_usuario = id_usuario
        self.calificacion = calificacion
        self.comentario = comentario
        self.animo = animo


destino1 = DestinoCulinario(
    id=1,
    nombre="Destino Culinario",
    tipo_cocina="Italiana",
    ingredientes=["Ingrediente1", "Ingrediente2", "Ingrediente3"],
    precio_minimo=10.99,
    precio_maximo=29.99,
    popularidad=4.5,
    disponibilidad=True,
    id_ubicacion=123,
    imagen="https://example.com/imagen_destino_culinario.jpg"
)

actividad1 = Actividad(
    id=1,
    nombre="Espectáculo de magia",
    destino_id=10,
    hora_inicio="2023-07-04T09:00:00"
)

ruta_visita1 = RutaVisita(
    id=1,
    nombre="Ruta gastronómica",
    destinos=[10, 20, 30]
)

ubicacion1 = Ubicacion(
    id=1,
    direccion="Calle Principal 123",
    coordenadas=[40.7128, -74.0060]
)

usuario1 = Usuario(
    id=1,
    nombre="Juan",
    apellido="Pérez",
    historial_rutas=[1, 2, 3]
)

review1 = Review(
    id=1,
    id_destino=10,
    id_usuario=1,
    calificacion=5,
    comentario="Excelente comida y servicio.",
    animo="Positivo"
)

# Acceder a los atributos de los objetos
print(destino1.id)
print(destino1.nombre)
print(destino1.tipo_cocina)
print(destino1.ingredientes)
print(destino1.precio_minimo)
print(destino1.precio_maximo)
print(destino1.popularidad)
print(destino1.disponibilidad)
print(destino1.id_ubicacion)
print(destino1.imagen)

print(actividad1.id)
print(actividad1.nombre)
print(actividad1.destino_id)
print(actividad1.hora_inicio)

print(ruta_visita1.id)
print(ruta_visita1.nombre)
print(ruta_visita1.destinos)

print(ubicacion1.id)
print(ubicacion1.direccion)
print(ubicacion1.coordenadas)

print(usuario1.id)
print(usuario1.nombre)
print(usuario1.apellido)
print(usuario1.historial_rutas)

print(review1.id)
print(review1.id_destino)
print(review1.id_usuario)
print(review1.calificacion)
print(review1.comentario)
print(review1.animo)